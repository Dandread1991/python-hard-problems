# Samir begins this time with all clean t-shirts
# Each day Samir wears a clean shirt, which becomes dirty
# Each day Samir goes to a conference, he gets one new clean shirt
# When Samir has no clean t-shirts, he does his laundry,
# which makes all his t-shirts clean again


def num_laundry_days(num_shirts, num_days, event_days):
    total_laundry_count = 0
    day_list = []
    shirts = num_shirts
    dirty_shirts=0
    for i in range(num_days): #initialize total days
        day_list.append(0)
    for i in range(num_shirts): #populate starter shirts
        day_list[i]=1
    for i,num in enumerate(event_days):
        event_days[i]=num-1      #adjust for 0-index array

    print(day_list,event_days,shirts)
    n=len(day_list)
    for index,day in enumerate(day_list):
        print("day",index+1, shirts,dirty_shirts)
        if shirts >= 1:
            print("Clean Shirt is now dirty")
            shirts-=1
            dirty_shirts+=1
            print("dirty",dirty_shirts)
        if shirts==0 and index !=day_list[n-1]:
            dirty_shirts,shirts=do_laundry(dirty_shirts,shirts)
            total_laundry_count+=1
        if index in event_days:
            print("event day, new shirt",shirts+1)
            shirts+=1

    return total_laundry_count

def do_laundry(dirty_shirts,shirts):
    shirts = dirty_shirts
    dirty_shirts=0
    print(f"shirts cleaned, now have {shirts} shirts")






    return dirty_shirts,shirts

# test = 1,10,[3,7]
# print(num_laundry_days(*test))

# test = 1, 10, [10]
# print(num_laundry_days(*test))

# test = 1, 10, [2, 5, 9]
# print(num_laundry_days(*test))
