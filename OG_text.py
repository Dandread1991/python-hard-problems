def translate(words,digits):
    final_word_count = 0
    lst = [int(x) for a,x in enumerate(str(digits))]

    telephone_code = {
            2:["A","B","C"],
            3:["D","E","F"],
            4:["G","H","I"],
            5:["J","K","L"],
            6:["M","N","O"],
            7:["P","Q","R","S"],
            8:["T","U","V"],
            9:["W","X","Y","Z"]


    }

    #print(lst)
    #print(words)
    #for num in lst:
    #   print(telephone_code.get(num))
    i=0

    for word in words:
        s=''
        #print("word",word)
        for letter in word:
            #print("letter",letter)
            for num in lst:
                #print("num",num)
                x=telephone_code.get(num)
                #print(x)
                if letter in x:
                   # print(letter)
                    s+=letter
                    break
            if s==word:
                final_word_count+=1
                #print(s,word,f"found {final_word_count}")

    return final_word_count




test = ["ARROW", "BOTOX", "DOMES", "FOODS"],26869
print(translate(*test))
