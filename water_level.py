def tide_difference(measurements):
    difference = 0
    n=len(measurements)
    max_val = max(sorted(measurements))
    min_val = min(sorted(measurements))
 #   print(min_val,max_val)

    min_index = measurements.index(min_val)
    max_index = measurements.index(max_val)

    if max_index == 0 :
        return None
    test = min_val
    for num in range(min_index,n):
   #     print(measurements[num-1])
        if measurements[num] < test:
            return None
        else:
            test = measurements[num]
            if test == max_val:
                break
    difference = max_val - min_val



   # print(min_index,max_index)
    return difference



# test1 = [8, 1, 2, 6, 4, 5, 10, 7] #None
# test2 = [9, 1, 2, 3, 4, 19, 20, 8] #19
# test3 = [1, 2, 3, 4, 5, 6, 7] #6
# test4 = [7, 6, 5, 4, 3, 2, 1] #None
# test5 = [1, 2, 3, 4, 5, 11, 8] #10
# test6 = [8, 1, 2, 3, 4, 5, 10] #9

# print(tide_difference(test6))
