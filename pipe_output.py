def pipe_outputs(num_pipes,steps):
    print(num_pipes,steps)
    pipe_list = []

    for i in range(num_pipes):
        pipe_list.append(8)
    for index,step in enumerate(steps):
        print("Step ",step)

        print(pipe_list)
        if len(step) >1:
            pipe_num=step[0]
            flow_val=step[1]
            print(pipe_num,flow_val)


            pipe_list = split_pipe(pipe_list,pipe_num,flow_val)
        else:
            pipe_num=step[0]
            flow_val=pipe_list[pipe_num-1]
            pipe_list=join_pipe(pipe_list,pipe_num,flow_val)
        print(pipe_list)
    return pipe_list


def split_pipe(pipe_list,split_pipe_num,flow_val):
    print(f"Splitting pipe {split_pipe_num} with flow {flow_val} into pipe {split_pipe_num+1}")
    n=len(pipe_list)
    pipe_list.append(pipe_list[n-1])



    new_flow=pipe_list[split_pipe_num-1]-flow_val
    pipe_list[split_pipe_num]=0
    pipe_list[split_pipe_num-1]=0
    pipe_list[split_pipe_num-1]=flow_val
    pipe_list[split_pipe_num]=new_flow



    return pipe_list

def join_pipe(pipe_list,join_pipe_num,flow_val):
    print(f"Joining pipe {join_pipe_num} to {join_pipe_num+1} with flows {flow_val} and {pipe_list[join_pipe_num]}")
    pipe_list[join_pipe_num-1]+=pipe_list[join_pipe_num]
    pipe_list=delete_pipe(pipe_list,join_pipe_num+1)

    return pipe_list

def new_pipe(pipe_list,pipe_num):

    return pipe_list

def delete_pipe(pipe_list,pipe_to_delete):
    #print(f"Deleting {pipe_to_delete}") delete pipe and renumber list
    n=len(pipe_list)
    for i in range(pipe_to_delete-1,n-1):
        pipe_list[i]=pipe_list[i+1]

    pipe_list.pop()

    return pipe_list

test = 3, [[2,4],[1],[1],[1,2]]
print(pipe_outputs(*test))

test = 3, [[2], [1]]
print(pipe_outputs(*test))

test =1, [[1, 4], [2, 2], [1, 2]]
print(pipe_outputs(*test))
